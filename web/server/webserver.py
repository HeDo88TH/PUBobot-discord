import asyncio

from quart import Quart
from quart_discord import DiscordOAuth2Session
from discord.ext import ipc
import redis
import sentry_sdk
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware

from .blueprints import auth, events, demo, websocket as websocket_bp
from .config import Config


def run(client_config):
    app = Quart(__name__)

    if hasattr(client_config, "SENTRY_DSN"):
        sentry_sdk.init(dsn=client_config.SENTRY_DSN)
        SentryAsgiMiddleware(app.asgi_app)

    app.config.from_object(Config)
    app.redis_client = redis.from_url(app.config["REDIS_URL"])

    # Required to access BOT resources.
    # app.config["DISCORD_BOT_TOKEN"] = "..."

    discord = DiscordOAuth2Session(app)
    app.register_blueprint(auth.app)
    app.register_blueprint(events.app)
    app.register_blueprint(demo.app)
    app.register_blueprint(websocket_bp.app)

    IPC = ipc.Client(
        host="127.0.0.1",
        port=2300,
        secret_key=client_config.IPC_SECRET
    )  # These params must be the same as the ones in the client

    app.connected_websocket_clients = set()

    # https://quart-discord.readthedocs.io/en/stable/introduction.html#basic-usage
    # https://flask-caching.readthedocs.io/en/latest/

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    try:
        app.ipc_client = loop.run_until_complete(
            IPC.start(loop=loop))  # `Client.start()` returns new Client instance or None if it fails to start
        app.run(
            loop=loop,
            port=app.config.get('PORT'),
            certfile=app.config.get('CERTFILE'),
            keyfile=app.config.get('KEYFILE'),
            debug=app.config.get('DEBUG'),
        )
    except RuntimeError as e:
        print("is pubobot.py with ipc server running?")
        raise e
    finally:
        loop.run_until_complete(app.ipc_client.close())  # Closes the session, doesn't close the loop
        loop.close()
