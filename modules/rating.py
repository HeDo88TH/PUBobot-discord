import itertools
import math
from typing import List, Tuple, Dict

import trueskill

UserId = int
Quality = float


def get_balanced_teams(players: Dict[UserId, trueskill.Rating]) -> Tuple[Tuple[List[UserId], List[UserId]], Quality]:
    num_players = len(players)
    best_teams = [], []
    max_quality: Quality = float('-inf')

    user_ids = set(user_id for user_id in players)

    # comb(N, N/2)/2!
    for alpha_user_ids in itertools.islice(itertools.combinations(user_ids, num_players // 2), 0,
                                           math.comb(num_players, num_players // 2) // 2):
        beta_user_ids = user_ids - set(alpha_user_ids)
        teams = alpha_user_ids, beta_user_ids
        quality = trueskill.quality(list(map(lambda team: [players[user_id] for user_id in team], teams)))
        if quality > max_quality:
            best_teams = teams
            max_quality = quality

    return best_teams, max_quality


def sort_players_in_teams(players: Dict[UserId, trueskill.Rating], teams: Tuple[List[UserId], List[UserId]]):
    return tuple(map(lambda team: sorted(team, key=lambda user_id: trueskill.expose(players[user_id]), reverse=True), teams))
