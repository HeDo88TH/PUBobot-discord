import time

from modules import console
from sentry_sdk import capture_exception

TASK_NAMES = {
	'add_in': lambda member, channel: f"{member.id}_{channel.id}_addin"
}


def init():
	global tasks, next_task
	tasks = dict()
	next_task = False


def run(frame_time):
	if next_task:
		if frame_time > tasks[next_task][0]:
			current_task = tasks.pop(next_task)
			try:
				current_task[1]()
			except Exception as e:
				console.display("SCHEDULER| ERROR: Task function failed @ {0} {1}, Exception: {2}".format(current_task[1], current_task[2], e))
				capture_exception(e)
			define_next_task()


def add_task(name, delay, callback, on_cancel=None, data=None) -> None:
	"""
	add task to scheduler to be called after delay
	@param name: should be unique to avoid conflicts
	@param delay: in seconds
	@param callback: callback after delay
	@param on_cancel: callback when task is canceled (e.g. users cancels add_in)
	@param data:
	"""
	tasks[name] = [time.time() + delay, callback, on_cancel, data]  # time to run task, func
	define_next_task()


def exec_on_cancel(task):
	if len(task) > 2 and task[2] is not None:
		try:
			task[2]()
		except Exception as e:
			console.display(f"SCHEDULER| ERROR: Task on_cancel failed @ {task[1]}, Exception: {e}")
			capture_exception(e)


def task_exists(name):
	return name in tasks


def cancel_task(name):
	task = tasks.pop(name)
	exec_on_cancel(task)
	if next_task == name:
		define_next_task()


def define_next_task():
	global next_task
	if len(list(tasks.keys())) > 0:
		first_task = min(list(tasks.items()), key=lambda x: x[1][0])
		if next_task != first_task[0]:
			next_task = first_task[0]
	else:
		next_task = False
