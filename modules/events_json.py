def member_list_gen(member_list):
    if member_list is None:
        return []
    return [{'id': str(member.id), 'nick': member.display_name} for member in member_list]


def pickup_change_json(pickup):
    return {
        'channel_id': str(pickup.channel.id),
        'name': pickup.name,
        'size': pickup.cfg['maxplayers'],
        'players': member_list_gen(pickup.players),
    }


def pickup_json(pickup, user_id):
    return {
        'channel_id': str(pickup.channel.id),
        'name': pickup.name,
        'size': pickup.cfg['maxplayers'],
        'players': member_list_gen(pickup.players),
        'channel_name': pickup.channel.name
    }


def match_json(match):
    # TODO: why is id str???
    # TODO: send channel link?, add id, int(start_time), unpicked_maps
    return {
            'id': match.id,
            'channel_id': str(match.channel.id), 'pickup': match.pickup.name,
            'players': member_list_gen(match.players),
            'state': match.state,
            'unpicked': [] if match.unpicked is None else member_list_gen(match.unpicked),
            'alpha_draw': match.alpha_draw,
            'beta_draw': match.beta_draw,
            'require_ready': match.require_ready,
            'alpha_team': member_list_gen(match.alpha_team),
            'beta_team': member_list_gen(match.beta_team),
            'ranked': match.ranked,
            'ranks': match.ranks if match.ranked else None,
            'start_time': int(match.start_time),
            'unpicked_maps': match.unpicked_maps,
            'map_pick_order': match.map_pick_order,
            'picked_maps': match.picked_maps,
            'maps': match.maps,
            'players_ready': list(match.players_ready) if match.state == 'waiting_ready' else [],
            'pick_order': match.pick_order,
            'pick_step': match.pick_step if hasattr(match, 'pick_step') else None,
            'server': match.server,
            'winner': match.winner
            }


def map_pick_json(match):
    return {
        'id': match.id,
        'players': member_list_gen(match.players),
        'unpicked_maps': match.unpicked_maps,
        'picked_maps': match.picked_maps,
        'map_pick_order': match.map_pick_order
    }


def player_pick_json(match):
    return {
        'id': match.id,
        'players': member_list_gen(match.players),
        'alpha_team': member_list_gen(match.alpha_team),
        'beta_team': member_list_gen(match.beta_team),
        'pick_step': match.pick_step,
        'unpicked': member_list_gen(match.unpicked)
    }


def set_ready_json(match):
    return {
        'id': match.id,
        'players': member_list_gen(match.players),
        'players_ready': [str(element) for element in match.players_ready]
    }
