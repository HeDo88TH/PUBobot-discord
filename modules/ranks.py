import bisect
from tabulate import tabulate


class Ranks(dict):

    def __init__(self, *args):
        super().__init__(*args)
        self.check()
        self._sorted_keys = sorted(list(self.keys()))
        self._count = len(self._sorted_keys)

    def find_key_by_rating(self, rating: int) -> int:
        # https://docs.python.org/3/library/bisect.html#searching-sorted-lists
        i = bisect.bisect_right(self._sorted_keys, rating)
        if i:
            return self._sorted_keys[i - 1]
        else:
            return self._sorted_keys[0]

    @classmethod
    def from_json_dict(cls, json_dict):
        """
        converts string keys to integers and returns Ranks instance
        """
        try:
            return cls({int(k): v for (k, v) in json_dict.items()})
        except ValueError:
            raise AssertionError("Rank keys must be integers")

    def find_by_rating(self, rating: int, *, long=False, wrap=True) -> str:
        values = self[self.find_key_by_rating(rating)]
        if long and len(values) > 1:
            rank_str = values[1]
        else:
            rank_str = values[0]
        return f"({rank_str})" if wrap else rank_str

    def check(self):
        assert len(self.keys()) > 0, "There must be at least 1 rating key"

        for key in self.keys():
            assert (isinstance(key, int)), 'Rank keys must be integers'
            assert (isinstance(self[key], list)), 'Rank values must be lists'
            assert (isinstance(self[key], list) and all(
                (isinstance(el, str) for el in
                 self[key]))), 'Rank values must be list of strings'
            assert 1 <= len(self[key]) <= 2, 'Rank value list must contain 1 or 2 strings'
        return self

    def rank_update_message(self, old_rating, new_rating):
        dir = "up" if old_rating < new_rating else "down"
        rv = f" has ranked {dir} from "
        if len(self[self.find_key_by_rating(old_rating)]) > 1:
            rv += self.find_by_rating(old_rating, wrap=False, long=True) + " "
        rv += self.find_by_rating(old_rating) + " to "
        if len(self[self.find_key_by_rating(new_rating)]) > 1:
            rv += self.find_by_rating(new_rating, wrap=False, long=True) + " "
        rv += self.find_by_rating(new_rating)
        return rv

    def to_markdown_table(self) -> str:
        return (
            "```\n"
            f"{tabulate([[key, self[key][0]] + ([self[key][1]] if len(self[key]) > 1 else []) for key in reversed(self._sorted_keys)], tablefmt='presto')}\n"
            "```"
        )


default_ranks = Ranks({
    2000: ["★"],
    1950: ["A+"],
    1900: ["A"],
    1850: ["A-"],
    1800: ["B+"],
    1750: ["B"],
    1700: ["B-"],
    1650: ["C+"],
    1600: ["C"],
    1550: ["C-"],
    1500: ["D+"],
    1450: ["D-"],
    1400: ["E+"],
    1350: ["E"],
    1300: ["E-"],
    1200: ["F+"],
    1100: ["F"],
    1000: ["F-"],
    0: ["G"]
})

custom_ranks = Ranks({
    2000: ["🍆", "Supreme Leader"],
    1900: ["🛐", "God"],
    1800: ["👑", "Legend"],
    1700: ["\N{fire}\N{fire}\N{fire}", "Elite 3"],
    1650: ["\N{fire}\N{fire}", "Elite 2"],
    1600: ["\N{fire}", "Elite 1"],
    1550: ["\N{Gem Stone}+", "Diamond 2"],
    1500: ["\N{Gem Stone}-", "Diamond 1"],
    1450: ["\N{Trident Emblem}+", "Gold 2"],
    1400: ["\N{Trident Emblem}-", "Gold 1"],
    1350: ["🦾+", "Silver 2"],
    1300: ["🦾-", "Silver 1"],
    1250: ["\N{jack-o-lantern}+", "Bronze 2"],
    1200: ["\N{jack-o-lantern}-", "Bronze 1"],
    1150: ["🥔", "Potat"],
    1100: ["🍠", "Sweet Potat"],
    1050: ["🥔🍠🥔", "Ultimate Potat"],
    1000: ["🧟", "Zombie"],
    0: ["👶", "Harmless"]
})

christmas_ranks = Ranks({
    2000: ["🌟", "Star"],
    1900: ["🍾", "Champagne"],
    1800: ["🍰", "Party Cake"],
    1700: ["🦌🎅🦌", "Ultimate Santa"],
    1650: ["🎄🎅", "Elite Santa"],
    1600: ["🎅", "Santa Claus"],
    1550: ["⛄+", "Snowman 2"],
    1500: ["⛄-", "Snowman 1"],
    1450: ["🎁+", "Present 2"],
    1400: ["🎁-", "Present 1"],
    1350: ["❄+", "Snowflake 2"],
    1300: ["❄-", "Snowflake 1"],
    1250: ["🧨+", "Cracker 2"],
    1200: ["🧨-", "Cracker 1"],
    1150: ["🍬", "Delicious Candy"],
    1100: ["🍬🍬", "Sour Candy"],
    1050: ["🍬🍬🍬", "Sticky Candy"],
    1000: ["🧚", "Angel"],
    0: ["👼", "Baby Angel"]
})
