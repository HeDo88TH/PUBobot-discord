import re
from typing import List, Optional
from random import choices, sample

from modules.exceptions import WrongTimeFormatException
from arrow.locales import EnglishLocale
import itertools
import math
import trueskill
import client_config


def weighted_sample_without_replacement(population, weights, k):
    if sum(i>0 for i in weights) < k:
        return sample(population, k)
    weights = list(weights)
    positions = range(len(population))
    indices = []
    while True:
        needed = k - len(indices)
        if not needed:
            break
        for i in choices(positions, weights, k=needed):
            if weights[i]:
                weights[i] = 0.0
                indices.append(i)
    return [population[i] for i in indices]


def parse_time_input(time_list):
    time_int = 0
    for i in time_list:  # convert given time to float
        try:
            num = int(i[:-1])  # the number part
            if i[-1] == 'd':
                time_int += num * 3600 * 24
            elif i[-1] == 'h':
                time_int += num * 3600
            elif i[-1] == 'm':
                time_int += num * 60
            elif i[-1] == 's':
                time_int += num
            else:
                raise ValueError
        except (IndexError, ValueError):
            raise WrongTimeFormatException(i)
    return time_int


def split_large_message(text, delimiter="\n", charlimit=1999):
    templist = text.split(delimiter)
    tempstr = ""
    result = []
    print(templist)
    for i in range(0, len(templist)):
        tempstr += templist[i]
        msglen = len(tempstr)
        if msglen >= charlimit:
            raise Exception("Text split failed!")
        elif i + 1 < len(templist):
            tempstr += delimiter
            if msglen + len(templist[i + 1]) >= charlimit - 2:
                result.append(tempstr)
                tempstr = ""
        else:
            result.append(tempstr)
    return result


def int_to_emoji(number):
    digit_emojis = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
    return ''.join([':' + digit_emojis[int(digit)] + ':' for digit in str(number)])


def get_id_from_mention(mention: str):
    return int(re.match(r"^<@!?(\d+)>$", mention).group(1))


def get_badge(user_id: int) -> str:
    badge = ""
    if hasattr(client_config, 'DONORS') and user_id in client_config.DONORS:
        badge += "⭐"
    if hasattr(client_config, 'CONTRIBUTORS') and user_id in client_config.CONTRIBUTORS:
        badge += "🔧"
    return badge


def member_list(players, *, separator='/', member_info_dict=None) -> str:

    def nick_str(player):
        if member_info_dict is not None:
            if player.id in member_info_dict:
                return get_badge(player.id) + member_info_dict[player.id]['nick']
            else:
                return format_member_nick(player)
        else:
            return format_member_nick(player)
    return separator.join([f"`{nick_str(player)}`" for player in players])


def format_member_nick(member) -> str:
    return get_badge(member.id) + (member.display_name).replace("`", "")


class EnLocale(EnglishLocale):
    names = ['_en']

    timeframes = {
        "now": "just now",
        "second": "1s",
        "seconds": "{0}s",
        "minute": "1m",
        "minutes": "{0}m",
        "hour": "1h",
        "hours": "{0}h",
        "day": "1d",
        "days": "{0}d",
        "week": "1w",
        "weeks": "{0}w",
        "month": "a month",
        "months": "{0} months",
        "year": "a year",
        "years": "{0} years",
    }


def trueskill_win_probability(team1, team2):
    """
    https://trueskill.org/#win-probability
    win probability of team1
    :param team1:
    :param team2:
    :return: float in range 0.0 - 1.0
    """
    delta_mu = sum(r.mu for r in team1) - sum(r.mu for r in team2)
    sum_sigma = sum(r.sigma ** 2 for r in itertools.chain(team1, team2))
    size = len(team1) + len(team2)
    denom = math.sqrt(size * (trueskill.BETA * trueskill.BETA) + sum_sigma)
    ts = trueskill.global_env()
    return ts.cdf(delta_mu / denom)


def split(string: Optional[str]) -> List:
    if string is None:
        return []
    return [token.strip() for token in string.split(',')]
