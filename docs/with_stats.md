```
pickup      is player1 with player2 in team    result    count
--------  ---------------------------------  --------  -------
6vs6                                      0       0          8
6vs6                                      0       0.5        2
6vs6                                      0       1         13
6vs6                                      1       0         16
6vs6                                      1       1         14
```
Rows explanation:
- player1 lost against player2 8 times
- player1 draw against player2 2 times
- player1 won against player2 13 times
- player1 lost with player2 16 times
- player1 won with player2 14 times


```
pickup      is player1 with player2 in team    is player1 with player3 in team    result    count
--------  ---------------------------------  ---------------------------------  --------  -------
6vs6                                      0                                  0         1        1
6vs6                                      0                                  1         0        2
6vs6                                      0                                  1         1        1
6vs6                                      1                                  0         0        2
6vs6                                      1                                  0         1        4
6vs6                                      1                                  1         0        1
6vs6                                      1                                  1         1        2
```