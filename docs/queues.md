# Queues

Brief explanation of how queueing up for matches work in this bot.

## Pickup queue

If some channel has some pickup configured with certain size, then people can add to its queue by typing `++`/`+<pickup_name>` and when queue is filled, match is started and queue is emptied.

Example:
```
Bot: [1v1 (0/2)]
Alice: ++
Bot: [1v1 (1/2)]
Greg: ++
Bot: [1v1 (0/2)] 
Bot: Pickup started!
```

## 🔂: Additional queue for players in active match to be re-added back when match finishes
If someone is in active match and tries to add to any queue, (s)he will be added to special queue that will add him(her) at the end of pickup queue when match is finished properly. This queue is represented with 🔂.
This queue lets others expect that its participant will be added back to pickup queue, otherwise it might seem to them that because queue is empty another match is not happening soon.

Order of filling the pickup queue can be configured to be random with `requeue_random` channel configuration variable.

Example:

```
Bot: [2v2 (2/4)]
Alice: ++
Bot: [2v2 (3/4)]
Greg: ++
Bot: [2v2 (0/4)]
Bot: Pickup started!
Greg: ++
Bot: [2v2 (0/4) 🔂1]
Eve: ++
Bot: [2v2 (1/4) 🔂1]
Alice: !reportlose
Bot: Match was finished ...
Greg: !who
Bot: [2v2 (2/4) Eve Greg]      # note that Greg was added after Eve, otherwise players in match could lock everyone else from playing
```
