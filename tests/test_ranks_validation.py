from modules.ranks import Ranks  # default_ranks, etc. are validated during import

invalid = [
    {},
    {
        'a': ['a']
    },
    {
        1: 'a'  # must be list
    },
    {
        1: []  # list must contain 1 or 2 elements
    },
    {
        1: ['a', 1]  # list element must be str
    }
]

for rank_dict in invalid:
    try: # try invalid
        Ranks(rank_dict)
    except AssertionError as exc:
        pass
    else:
        raise AssertionError(f"AssertionError for {rank_dict} expected")
