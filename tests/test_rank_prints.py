from modules.ranks import Ranks, default_ranks, custom_ranks, christmas_ranks

custom_rank = Ranks({
    2000: ["S", "Supreme"],
    1000: ["😎", "cool guy"],
    500: ["E"],
    0: ["F", "FFFFF"]
})

for r in [default_ranks, custom_ranks, christmas_ranks, custom_rank]:
    print(r.to_markdown_table())