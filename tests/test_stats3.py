import os
from modules import stats3, config, bot


class DictMock:
    def __init__(self, d):
        self.__dict__ = d


config.init()
db_file_name = "test.sqlite3"
db_exists = os.path.isfile(db_file_name)
if db_exists:
    os.unlink(db_file_name)
stats3.init(db_file_name=db_file_name)

CHANNEL_ID = 1
discord_channel = DictMock({'id': CHANNEL_ID, 'name': 'test-channel', 'guild': DictMock({'name': 'test-guild'})})
channel_cfg = stats3.new_channel(1, "test-guild", CHANNEL_ID, "test-channel", 1)
channel_cfg['ranked'] = 1
channel_cfg['ranked_streaks'] = 0
channel_cfg['ranked_calibrate'] = 0
channel = bot.Channel(discord_channel, channel_cfg)
pickup_cfg = stats3.new_pickup(CHANNEL_ID, "1v1", 2)
pickup = bot.Pickup(channel, pickup_cfg)

players = (
    DictMock({'id': 1, 'nick': '1'}),
    DictMock({'id': 2, 'nick': '2'}),
    DictMock({'id': 3, 'nick': '3'}),
    DictMock({'id': 4, 'nick': '4'}),
)

match = bot.Match(pickup, [players[0], players[1]], "alpha")
match.panzers = []
stats3.register_pickup(match)

match = bot.Match(pickup, [players[0], players[3]], "draw")
match.panzers = []
stats3.register_pickup(match)

match = bot.Match(pickup, [players[0], players[1]], "beta")
match.panzers = [players[0], players[1]]
stats3.register_pickup(match)

match = bot.Match(pickup, [players[0], players[2]], "draw")
match.panzers = [players[0], players[2]]
stats3.register_pickup(match)

lb, _ = stats3.get_rank_details(CHANNEL_ID, 1, win_loss_only=False)
print(dict(lb))
assert lb['wr'] == 0.75
assert lb['pfwr'] == 0.25
assert lb['place'] == 1
assert lb['pfplace'] == 4
lb, _ = stats3.get_rank_details(CHANNEL_ID, 1, win_loss_only=True)
print(dict(lb))
assert lb['place'] == 1
assert lb['pfplace'] == 3

lb, _ = stats3.get_rank_details(CHANNEL_ID, 2, win_loss_only=False)
print(dict(lb))
assert lb['wr'] == 0
assert lb['pfwr'] == 1
assert lb['place'] == 4
assert lb['pfplace'] == 1

lb, _ = stats3.get_rank_details(CHANNEL_ID, 2, win_loss_only=True)
print(dict(lb))
assert lb['place'] == 3
assert lb['pfplace'] == 1

lb, _ = stats3.get_rank_details(CHANNEL_ID, 3, win_loss_only=False)
print(dict(lb))
assert lb['wr'] == 0
assert lb['pfwr'] == 0.5
assert lb['place'] == 2
assert lb['pfplace'] == 2

lb, _ = stats3.get_rank_details(CHANNEL_ID, 3, win_loss_only=True)
print(dict(lb))
assert lb['place'] == 4
assert lb['pfplace'] == 2

lb, _ = stats3.get_rank_details(CHANNEL_ID, 4, win_loss_only=False)
print(dict(lb))

assert lb['wr'] == 0.5
assert lb['pfwr'] == 0
assert lb['place'] == 3
assert lb['pfplace'] == 3

lb, _ = stats3.get_rank_details(CHANNEL_ID, 4, win_loss_only=True)
print(dict(lb))
assert lb['place'] == 2
assert lb['pfplace'] == 4

lb, _ = stats3.get_ladder(CHANNEL_ID, 0, win_loss_only=False, pf=False, min_matches=None)
pf_lb, _ = stats3.get_ladder(CHANNEL_ID, 0, win_loss_only=False, pf=True, min_matches=None)
print("lb", [dict(row) for row in lb])
print("pf_lb", [dict(row) for row in pf_lb])
assert lb[0]['nick'] == '1'
assert lb[0]['wr'] == 0.75
assert lb[1]['nick'] == '3'
assert lb[1]['wr'] == 0
assert lb[2]['nick'] == '4'
assert lb[2]['wr'] == 0.5
assert lb[3]['nick'] == '2'
assert lb[3]['wr'] == 0

assert pf_lb[0]['nick'] == '2'
assert pf_lb[0]['wr'] == 1
assert pf_lb[1]['nick'] == '3'
assert pf_lb[1]['wr'] == 0.5
assert pf_lb[2]['nick'] == '1'
assert pf_lb[2]['wr'] == 0.25

lb, _ = stats3.get_ladder(CHANNEL_ID, 0, win_loss_only=True, pf=False, min_matches=None)
pf_lb, _ = stats3.get_ladder(CHANNEL_ID, 0, win_loss_only=True, pf=True, min_matches=None)
print("lb", [dict(row) for row in lb])
print("pf_lb", [dict(row) for row in pf_lb])
assert lb[0]['nick'] == '1'
assert lb[0]['wr'] == 0.75
assert lb[1]['nick'] == '4'
assert lb[1]['wr'] == 0.5
assert lb[2]['nick'] == '2'
assert lb[2]['wr'] == 0

assert pf_lb[0]['nick'] == '2'
assert pf_lb[0]['wr'] == 1
assert pf_lb[1]['nick'] == '3'
assert pf_lb[1]['wr'] == 0.5
assert pf_lb[2]['nick'] == '1'
assert pf_lb[2]['wr'] == 0.25

stats3.close()
