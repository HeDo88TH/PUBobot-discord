from modules.ranks import Ranks, default_ranks

r = Ranks(default_ranks)

for rating, expected_key in [
    (1400, 1400),
    (50, 0),
    (3000, 2000),
    (1345, 1300),
    (-100, 0)
]:
    res = r.find_key_by_rating(rating)
    assert res == expected_key, f"{rating} maps to {res} instead of {expected_key}"
