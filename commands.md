## Available commands:
### ACTIONS:
- `!add *pickup*[ *pickup* ..]` or `+*pickup*[ *pickup* ..]` or `++ *pickup*[ *pickup* ..]` - adds you to specified pickups.   
- `!add` or `++` - adds you to all pickups in channel.   
- `!remove *pickup*[ *pickup* ...]` or `-*pickup*[ *pickup* ..]` or `-- *pickup*[ *pickup* ..]` - removes you from specified pickups.   
- `!remove` or `--` or `-` - removes you from all pickups.   
- `!add_in *time* *[pickup ..]*` - schedule add. Without pickup argument the command will act as ++, so it might not always add - see `!add`.   
- `!add_in -` or `!add_in --` or `!add_in cancel` will cancel scheduled add_in task.   
- `!expire *time*` - Sets new time delay after you will be removed from all pickups, example: '!expire 1h 2m 3s'. Alias: `!remove_in`   
- `!default_expire *time*` - Set your personal default !expire time.   
- `!default_expire afk` - Will move user when AFK.   
- `!default_expire none` - Will use discord server(guild) configured global expire.   
- `!sub` - request sub for last game.   
- `!allowoffline` or `!ao` - gives you immune from getting removed by offline or afk statuses until a pickup with you starts. (done for mobile devices users).   
- `!subscribe *pickup*[ *pickup* ..]` - adds the promotion role of specified pickup(s) to you.   
- `!unsubscribe *pickup*[ *pickup* ..]` - removes the promotion role of specified pickup(s) from you.   
- `!web` - replies with public url of web interface  
- `!donate` - replies with `DONATE_REPLY` from client_config.py  
- `!tg` - telegram integration see [Telegram Notifications](docs/telegram.md)

### INFO:
- `!who [*pickup*[ *pickup* ...]]` - list of users added to a pickups.   
- `!who_all` - shows active queues, matches, parties in whole guild. Requires embed links permission. Alias: `!wa`   
- `!who_scheduled` - shows players who are scheduled to be added. Alias: `!ws`   
- `!pickups` - list of all pickups on the channel.   
- `!pickup_groups` - list of pickup groups configured on the channel.   
- `!expire` - shows you how much time left before you will be removed from all pickups.   
- `!add_in` - shows when you will be auto-added   
- `!lastgame [*mention* or *pickup*]` - show last pickup, or last pickup by specified argument.   
- `!top [*pickup*] [daily or weekly or monthly or yearly]` - shows most active players.   
- `!top maps [*pickup*] [daily or weekly or monthly or yearly] [-x]` - shows most played maps   
- `!activity [*pickup*] [daily or weekly or monthly or yearly] [-x]` - shows amount of games for specified timeframe  
- `!activity [*pickup*] by_hour` - shows amount of games per hour of day  
- `!stats [*nick* or *pickup*]` - shows you overall stats or stats for specified argument.   
- `!ip [*pickup* or default]` - shows you ip of last pickup or specified pickup.   
- `!map *pickup*` - print a random map for specified pickup.   
- `!maps *pickup*` - show all maps for specified pickup.  
- `!winner_team_stats [before=DD.MM.YYYY] [after=DD.MM.YYYY]` [mention] Examples: `!winner_team_stats before=31.7.2024`, `!winner_team_stats after=31.7.2024 @user`
- `!with_stats @player1 @player2 ...` - statistics of mentioned players playing together or against. Use `!with_stats` to print help.

### TEAMS PICKING:
- `!cointoss [heads or tails]` - toss a coin. Alias: `!ct`
- `!pick *mention* [mention, ...]` - pick a player(s) to your team. Alias: `!p`  
- `!put *mention* alpha|beta` - put player in specified team (available only for users with moderator or admin rights).     
- `!capfor alpha|beta` - become a captain of specified team.   
- `!teams` - show teams for current pickup.  
- `!pick_captains <mention1> <mention2>` - moderators can choose manually captains and reset picking if match is still in picking stage

### MAP PICKING:
- `!map_pick <number>`. Picks or bans map. Alias: `!m`
- *more info in CONFIGURATION VARIABLES section*  
- `!mappref map1:number1,map2:number2,...` sets map preferences, if automatic map picking is enabled by the channel. Numbers encoding: 1 - always, 2 - sometimes, 3 - never, 4 - don't know. Example: !mappref ice:1,beach:3,base:2  
- `!update_pref map_name preference` - updates a map preference, if automatic map picking is enabled by the channel. Options for preference are: always, sometimes, never, dunno. Example: !update_pref ice sometimes  
- `!myprefs` - shows selected map preferences

### RANKING
!leaderboard [page] [-a] or !lb [page] [-a] - show top players by rating. With `-a` argument displays streaks regardless of players activity, otherwise will only show players that were active in past 7 days.  
!trueskill [page] or !ts [page] - show top players by TrueSkill rating (mu-3*sigma). This rating is not affected by `!rank_reset`. [About TrueSkill](https://trueskill.org/). `role=<Role ID>` argument can be used to filter by role. E.g. `!ts 2 role=123456`   
!pflb [page] [-a] - shows top players sorted by pf rating (only on channels with separate pf role). With `-a` argument displays streaks regardless of players activity, otherwise will only show players that were active in past 7 days.     
!rank - show your rating stats.   
!reportlose or !rl - report loss on your current match (available for captains only).   
!reportdraw !draw or !rd - report draw on your current match (available for captains only, captain of other team has to confirm).   
!reportcancel or !rc - propose to cancel your current match (available for captains only).   
!matches - show all active matches on the channel.   
!ranks_table - show rank to ranks table.   
!streaks [-a] - displays winning and losing streaks. `!streaks -a` displays streaks regardless of players activity, otherwise will only show players that were active in past 7 days. The days are configurable with `streaks_active_past_days` [extension key](#extension-keys) 

##### For moderators and above:
- `!reportwin *match_id* alpha or beta or draw` - report win or draw on specified match for specified team. Alias: `!rw`  
- `!undo_ranks *match_id*` - undo all rating changes for a previously reported match.   
- `!seed *mention* [*mention*, ...] *rating*` - set specified player's rating points, also this will disable initial rating calibration for this user.   
- `!pfseed *mention* [*mention*, ...] *rating*` - set specified player's pf rating points, also this will disable initial rating calibration for this user. Alias: !seedpf   
- `!add_player *pickup_name* *mention* *mention* ...` - adds manually players to pickup
##### For admins:
- `!reset_ranks` - reset all rating data on the channel. **Warning, this action is irreversible!**   


### Game servers
Moderators can add game servers for ranked pickups, so bot suggests game server that isn't used.  
- `!servers` - list of game servers for discord server  
- `!add_server` - adds game server: `!add_server test.com:1234 {"pw":"password"}`
- `!edit_server` - edits game server: `!edit_server *server_id* test.com:1234 {"pw":"password"} ` 
- `!remove_server *server_id*` - removes server  
- `!pick_server [*pickup_name*]` - suggests server - doesn't actually use server, for testing what would bot suggest  
- `!propose_server [*server_id*]` - Propose server to be switched to, only for team captain.  
- `!accept_server` - accept proposed server, only for team captain.  
- `!set_server [*match_id*] [*server_id*]` - set game server for match by moderator.  

Set configuration variable **servers** to allow and sort servers for pickups or pickup channels.  
Example: There are 5 game servers, but for 1v1 pickup you want to allow only server with id 3 and 4: !set_pickups 1v1 servers 3,4

### Party system:

Party system enables player to create his own party, invite others to it and challenge other parties. As opposed to pickups, where one can't really choose who his teammates are.

- to enable use !set_default party 1
- party_number can be checked with !parties command
- the captain of party is its 1st player, if he leaves, 2nd player becomes captain
- party has to be filled to challenge other filled party
- party can only be challenged by one other party
- player can only be in one party per channel

#### Public party:  
- each pickup can have 1 public party, player can join it with `!public_party` or `!pp` command
- public party can't challenge regular party
- public party accepts challenge immediately without approval
- specific commands: `!public_party`, `!pp`

#### Pickup party:  
Pickup party is basically invite-only pickup. For example pickup party of 6vs6 pickup has capacity of 12 players. 
When party is full, its captain can start the pickup from the party with `!start_pickup` command. 
- specific commands: `!pickup_party`, `!start_party`

##### Commands:   
!party [*pickup_name*] - creates party with creator as captain    
!leave_party - removes you from party    
!accept_invite [*party_number*] or !ai    
!decline_invite [*party_number*] or !di  
!public_party [*pickup_name*] or !pp - adds you to public party  
!pickup_party [*pickup_name*] - creates pickup party
   
##### For party captains:    
!kick *mention* [*mention*, ...]  
!invite *mention* [*mention*, ...] or !i   
!challenge [party_number] or !c    
!accept_challenge or !ac    
!decline_challenge or !dc   
!start_party - starts pickup of pickup party  

##### For moderators and above:
!add_party *party_number* *mention* [*mention*, ...] - adds players to party    
!remove_party *mention* [*mention*, ...]  - removes players from party    
!create_party *pickup_name* *mention* [*mention*, ...] - creates regular party with players

### MODERATION:
!noadd [-anon] [-perm] (member_mention|member_id)+ [(channel_mention)+] [duration] [reason] - disallow user to play pickups.
  - *-anon* - makes noadd anonymous, should use it with channel mentions to noadd someone from some private channel.
  - *-perm* - makes noadd permanent
  - moderator level is checked in specified channels
  - example: !noadd -anon 123456789 123456788 #3v3 #6v6 3d rage-quit 
   
!noadds [-perm] [member_mention] [page_index] - show list of users who are disallowed to play pickups.
  - *-perm* - shows permanent or noadds longer than 3 months
  - member_mention - shows only noadds from that member
  - page_index - pagination, starts with 0, if used even inactive noadds are shown
   
!forgive mention [mention, ...] - allow user(s) from noadds list to play pickups.   
!phrase mention *text* - set specified reply for specified user after !add command.   
!remove_player mention - remove specified players from all pickups.   
!reset *pickup* - removes all players from all pickups or *pickup*.   
!start *pickup* - force a pickup to start with deficient players count.   
!cancel_match *match_id* | *mention* - cancel an active match, match_id can be found at the beginning of pickup start message. If you use mention, it will re-add everyone except the mentioned member back to pickup. alias: `!cm`   
!report_match *pickup* *time* *winner* *alpha_players* *beta_players* - manually report match example: !report_match 2v2 19.3.20-20:51 alpha *mention1* *mention2* *mention3* *mention4*; optional parameter "pfs" can be set at the end to signify first players of both entered teams played pf, i.e. "!report_match 2v2 3.19.20-20:51 alpha mention1 mention2 mention3 mention4 pfs" considers mention1 and mention3 to have played pf for their respective teams.   
!subfor *target* *substitute* ['redue'] - *target* replaces *substitute* in active match. Target can be user_id, part of nick or mention. If you use redue parameter, bot will create new teams after substitution but keep old maps/panzers.   
!redue - Recreates teams. Will only produce a different result if players are reseeded.   
!set_panzers *player1* *player2* - Sets the two players as panzers for the match.

### Member info
Moderators have ability to assign custom nick and countries to users. This overrides their discord display name in many places in place where teams are listed it adds the country flag emoji. This data is shared across whole discord server.
Commands:   
- `!get_member_info (mention|user_id)` - displays assigned nick and country. Alias: `!gmi`
- `!list_member_info [page number] [flag=(country iso code)]` - Lists assigned members info. Page numbers starts from 0. Optional flag=(iso code) argument can be used to filter by country. Alias: `!lmi` Examples: `!lmi 1 flag=nl`, `!lmi flag=pt`, `!lmi 2`
- `!set_member_info (mention|user_id) (nick|none) (country iso code|none)` - assigns nick and country to user. Using none as value for nick/ country won't ossign nick/country. Alias: `!smi` Examples: `!smi @User Nick us`, `!smi 1234567 Nick none`, `!smi @User none ca`
- `!delete_member_info (mention|user_id)` - Deletes user's member info data. Alias: `!dmi` Examples: `!dmi @User`, `!dmi 1234567`
- `!member_info_flags` - Displays list of countries sorted by assigned members

#### Status message:  
Status message shows pickups and matches from specified channels in embed format. To add status message type `!status_message channel [*channel, ...]`, where channel can be channel mention or id,  
 - this command is only possible with manage guild permission.
 - can subscribe to channels from different guilds
 - Example: `!status_message #tdm 1234657894 #ctf`
 - subscribed channels needs to have `public` configuration variable set to positive number - !set_default public 1
 - Parties aren't implemented yet.  
   
If you reply to the status message you can set its embed fields with json format:
 - Example: `{"color": 8311585,"title": "**GM Status:** "}` 
 - embed online tester: https://leovoel.github.io/embed-visualizer/

If you reply with `json` to the status message, bot will DM you the embed json.
Reply with `remove` to remove the status message.

### CONFIGURATION:
!enable_pickups - turn on the pickup bot on the channel.   
!disable_pickups - turn off the bot and delete all configurations/stats on the channel. bWarning, this action is irreversible!/b   
!add_pickups *name*:*players*[ *name*:*players* ...] - create new pickups.   
!remove_pickups *pickup*[ *pickup* ...] - delete specified pickups.   
!add_pickup_group *group_name* *pickup*[ *pickup*...] - create a pickup group which will contain specified pickups.
!remove_pickup_group *group_name* - delete specified pickup group.   
!reset_stats - delete all channel statistics.   
!cfg - show global channel configuration variables.   
!pickup_cfg *pickup* - show specified pickup configuration variables. Alias: `!pcfg`  
!set_ao_for_all *name* 0|1 - allow/disallow offline for all users of specific pickup kind.

!set_default variable value - set a global variable value. Available variables (incomplete list): admin_role, moderator_role, captains_role, prefix, default_bantime, ++_req_players, startmsg, submsg, ip, password, maps, pick_captains, pick_teams, promotion_role, promotion_delay, blacklist_role, whitelist_role, require_ready, ranked, ranked_calibrate, ranked_multiplayer, ranked_streaks, global_expire, match_livetime, initial_rating, help_answer, party, best_of, servers, hide_ranks, whitelist_msg, blacklist_msg, extension, min_matches_leaderboard, disallowed_cmds.   
- alias: `!set`

!set_pickups *pickup*[ *pickup*...] variable value - set variables for specified pickups. Available variables (incomplete list): maxplayers, startmsg, submsg, ip, password, maps, pick_captains, pick_teams, pick_order, promotion_role, blacklist_role, whitelist_role, captains_role, require_ready, ranked, help_answer, best_of, map_pick_order, map_pref_message, servers, whitelist_msg, blacklist_msg, extension, map_pick_vote, map_pick_vote_duration.   
- alias: `!setp`

!set_default_extension key value - set only extension value for specific key for channel. `none` value removes the key from extension.
- alias `!setex`
- example: `!setex ready_timeout 10`
- example: `!setex ready_timeout none`

!set_pickups_extension *pickup*[*pickup*,...] key value - set only extension value for specific key for pickups. `none` value removes the key from extension.
- alias `!setpex`
- example: `!setpex 6v6 ready_timeout 10`
- example: `!setpex 6v6 ready_timeout none`

Extensions can be also set through bot DM by specifying channel id after command e.g: `!setpex 123456789 1v1 ready_timeout 150`.
But currently can only be done by admin who did `!enable_pickups` or global admin configured in `client_config.ADMIN_IDS`.

##### CONFIGURATION VARIABLES:
* For any variable set 'none' value to disable.
* admin_role *role_name* - users with this role will have access to configuration and moderation commands.
* moderator_role *role_name* - users with this role will have access to moderation commands.
* captains_role *role_name* - random captains will be preferred to this role, also '!capfor' command will be only available for users with this role if its set.
* prefix *symbol* - set prefix before all bot's commands, default '!'.
* default_bantime *time* - set default time for !noadd command.
* ++_req_players *number* - set minimum pickup required players amount for '++' command or '!add' command without argument, so users won't add to 1v1/2v2 pickups unintentionally. Default value: 5.
* startmsg *text* - set message on a pickup start. Use %ip% and %password% in *text* to represent ip and password.
* start_pm_msg *text* - set private message on a pickup start. Use %pickup_name%, %ip%, %password% and %channel% to represent its values.
* submsg *text* - set message on !sub command. Use %pickup_name%, %ip%, %password% and %promotion_role% to represent its values.
* promotemsg *text* - set message on !promote command. Use %promotion_role%, %pickup_name% and %required_players% to represent its values.
* ip *text* - set ip which will be shown in startmsg, submsg and on !ip command.
* password *text* - set password which will be shown in startmsg, submsg and on !ip command.
* maps *map_name*[, *map_name*...] - set maps.
* pick_captains 0, 1, 2 or 3 - set if bot should suggest captains.
  * if variable ranked is 0:
    * 0 - doesn't suggest captains
    * 1 - picks captains randomly but with preference of player having captain role, always used with auto picking
    * 2 - picks captains randomly
  * if variable ranked is 1:
    * 0 - doesn't suggest captains
    * 1 - sorts players by player having captain role and player rank and picks two players from top
    * 2 - sorts players by player rank and picks random pair of adjacent players
    * 3 - sorts players by players who got more than 20 matches then player rank and picks 2 players from top 4. Also shuffles alpha and beta captain
    * 4 - picks captains randomly
    * 5 - sorts players by *captain role*. If there are less than 2 candidates then it will append 1 or 2 players by highest rank.
          Then it picks 2 candidates randomly.
    * 6 - sorts players by *captain role* and *rank*. If there are less than 2 candidates then it will append 1 or 2 players by highest rank.
          Then it picks random pair of *adjacent* players from candidates and shuffle the pair randomly.  
* pick_teams *value* -  set teams pick system the bot should use. Value must be in 'no_teams', 'auto' or 'manual'.
  * no_teams - bot will only print players list and captains if needed.
  * auto - bot will print teams balanced by rating on ranked pickups or random teams.
  * manual - users will have to pick teams using teams picking commands.
* team_emojis *emoji* *emoji* - set custom team emojis.
* team_names *alpha_name* *beta_name* - set custom team names (commands with team names will change accordingly).
* custom_ranks 0, 1, 2 - normal A/B/C ranks: 0, custom emoji ranks: 1, christmas emoji ranks: 2. See: [ranks.py](modules/ranks.py). For full customization set ranks extension.
* pick_order *order* - force specified teams picking order. Example value: 'abababba'.
* promotion_role *role_name* - set promotion_role to highlight on !promote and !sub commands.
* promotion_delay *time* - set time delay between !promote and !sub commands can be used.
* blacklist_role *role_name* - users with this role will not be able to add to specified pickups.
* whitelist_role *role_name* - only users with this role will be able to add to specified pickups.
* whitelist_msg *text* - custom bot reply when whitelist_role is set and player, that wants to add, doesn't have whitelist role. Placeholders: %whitelist_role%, %pickup_name%
* blacklist_msg *text* - custom bot reply when blacklist_role is set and player, that wants to add, have blacklist role. Placeholders: %blacklist_role%, %pickup_name%
* require_ready none or *time* - if set users will have to confirm themselves using '!ready' command.
* ranked 0 or 1 - enable rating system in the channel and make players have to report their matches. Setting ranked to 0 for pickup will disable removing players from other queues when pickup starts.
* ranked_calibrate 0 or 1 - set to enable rating boost on first 10 user's matches, default on. Only for 'set_default'.
* ranked_multiplayer 0 to 256 - change rating K-factor (gain/loss multiplyer), default 32. Only for 'set_default'.
* ranked_streaks 0/1/2 - set to enable ranked streaks
  * mode 1: starting from x1.5 for (3 wins/loses in a row) to x3.0 (6+ wins/loses in a row)
  * mode 2: starting with 3 game streaks, length of streak is added to elo gained, i.e. player on 5 game losing streak loses 5 extra elo points
* initial_rating *integer* - set starting rating for new players (default is 1400).
* global_expire *time*, afk or none - set default_expire value for users without personal settings.
* match_livetime *time* - set a time-limit before a match gets aborted as timed out.
* help_answer *text* - set an answer on !help command.
* party 0 or 1 - enable party system, see party system section
* best_of number - how many maps should be picked for match
* map_pick_order [A|B|a|b]+ - if set then captains pick/ban from maps OR *mappref* for automatic map picks according to preferences set by the players 
  * uppercase - pick, lowercase - ban
  * If there are less order characters than maps, then bot fills picked maps to match with best_of value
  * i.e. there are 7 maps, map_pick_order = abBA, best_of = 3: alpha captain bans 1st, beta bans, beta picks, alpha picks, bot randomly picks 1 map from 3 remaining
* map_pick_vote none or *int* - If set all players will vote for maps from x candidates where x is the value of map_pick_vote. The candidates are determined based on these factors:
  * if map_pick_order is set to mappref, the maps with the highest preferences become candidates
  * if mapprefs arent used but map_probabilities are set, candidates are randomely picked according to the set probabilities
  * otherwise maps are picked randomly from the mappool. 
* map_pick_vote_duration none or *time* - Duration of the map vote. If not set defaults to 120s.
* map_pref_message *text* - when map_pick_order is set to mappref, display given message to users who haven't set their preferences.
* servers - see Game servers section
* hide_ranks 0,1 or 2 (default 0)
  - hide ratings from users, but keeps them in the database. So ratings are still used for picking captains, teams. Ranked pickup with hidden ratings has to be finished with `!rl`, so players are kept in the active match. Use `!clear_rating_messages` to clear rating from message history of channel.
  - option 2 hides rating/ranks and order players by win ratio
* public 0 or 1 (default 0) - can this channel be shown in status message  
* requeue_random 0 or 1 (default 1) - shuffle players that re-queued from active match and are being added to the pickups when their match finishes  
* extension <json> - intended for special cases: example: {"multiple_captain_roles_v1":[123456789, 123456798]}, [Extension keys](#extension-keys)
* min_matches_leaderboard - minimum matches of player for `!leaderboard`, default 10
* disallowed_cmds *commands separated by comma* - disables commands in channel, except for channel admins

#### Extension keys:
* multiple_captain_roles_v1 - list of role IDs, that would be randomly picked to determine captain role when ranked == 1 and pick_captains == 5. If there are less than 2 candidates from picked role it will fall back to captains_role as if this extension wasn't set. Example: `!setex multiple_captain_roles_v1 [1234567,234567]`
* streaks_active_past_days - number of days - `!streaks` will filter players only that played in the channel at least once past given days. If it is set to `-1`, `!streaks` will act as `!streaks -a`. This key will is only for channel config. Example: `!setex streaks_active_past_days 30`
* ready_timeout - timeout in seconds in which player will not need to ready up again for match of the same pickup. Set to 0 to disable. Its 5 minutes by default.
* POST_EVENTS - sends HTTP POST events to url with headers - Example: `!setex POST_EVENTS {"url":"http://localhost:8080","headers":{"X-TEST":"test"}}`. The value is replaced with `REDACTED` in `!cfg` and `!pcfg` command replies for non-admin.
* re_requeue_emoji
* ranks - custom ranks table. `!ranks_table` command diplays them - Example: `!setex ranks {"2000": ["S", "Supreme"], "1000": ["😎", "cool guy"], "0": ["F"]}` - the second value in list (e.g. "Supreme", "cool guy") is only displayed in rank upgrade/downgrade message ("X has ranked up from cool guy (😎) to Supreme (S))
* send_rank_upgrades - enables rank upgrade message - enabled by default - to disable: `!setex send_rank_upgrades 0`
* send_rank_downgrades - enables rank downgrade message - disabled by default - to enable: `!setex send_rank_downgrades 1`
* short_queue_update - toggles queue update message to not include nicknames - to have short form queue updates: `!setex short_queue_update 1`
* auto_teams_use_elo - will use Elo instead of trueskill for automatic teams (`!set teams auto`). Elo will be also used if player count is bigger than 12 or when player count not even. To enable using Elo: `!setex auto_teams_use_elo 1`