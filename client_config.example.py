# discord.Client.intents.presences = ?
INTENTS_PRESENCES = True
INTENTS_MEMBERS = True
IPC_ENABLED = False  # for web
IPC_SECRET = "change me"
# WEB_URL = "http://localhost:5000"
# PUBLIC_WEB_URL = "https://gm.greatshot.xyz"
# SENTRY_DSN = "..."  # add dsn url and uncomment to enable sentry
# DONATE_REPLY = ""

ADMIN_IDS = {}  # set of discord user ids that will have full rights in bot

POST_EVENTS = False
"""
enable posting events to per pickup/channel configured url with POST_EVENTS extension, requests package required
"""

# TELEGRAM_TOKEN = '...'  # see docs/telegram.md
# TELEGRAM_BOT_LINK = 'https://t.me/...'